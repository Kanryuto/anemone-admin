const fs = require('fs');
const path = require('path');

const readFile = (file, extension) => {
  let content = fs.readFileSync(file);
  if (extension === "json") {
    return JSON.parse(content);
  }
  return content;
};

const readFiles = (dir) => {
  let resources = {};
  fs.readdirSync(dir)
    .filter((file) => {
      return path.join(__dirname, file) !== __filename && path.join(__dirname, file) !== path.join(__dirname, ".DS_Store");
    })
    .forEach((file) => {
      if (!path.extname(file)) {
        const childDir = path.basename(file);
        resources[childDir] = readFiles(path.join(dir, file));
        return;
      }
      const extension = path.extname(file).replace('.', '');
      const basename = path.basename(file, `.${extension}`);
      const object = readFile(path.join(dir, file), extension);
      resources[basename] = object;
    });
  return resources;
}

let resources = readFiles(__dirname);
module.exports = resources;
module.exports.default = resources;
